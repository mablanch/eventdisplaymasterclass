#!/usr/bin/env python

#
# Script to extract events from stripped DST to JSON format
#
#

import PartProp.Service
import os
import json
import sys
import math
from optparse import OptionParser

from Gaudi.Configuration import *
from Configurables import DaVinci
from AnalysisPython import Dir, Functors
from GaudiPython.HistoUtils import book
from GaudiPython.Bindings import gbl
try:
    from DaVinciTools import Tools as DVTools
except:
    from DaVinciPVTools import Tools as DVTools
from ROOT import Double
from ROOT import Math
from ROOT import pair
from ROOT import TMatrixDSym,TMatrixDSymEigen
from array import array

from Configurables import FilterDesktop
from Configurables import CombineParticles
from PhysSelPython.Wrappers import AutomaticData, Selection, SelectionSequence

from Configurables         import NoPIDsParticleMaker
from CommonParticles.Utils import *
from CommonParticles import StdNoPIDsPions
from Gaudi.Configuration   import *

#from ROOT import SMatrix
#from Gaudi.Math.GSL import EigenSystem
LHCb = gbl.LHCb
Track = LHCb.Track
XYZPoint= gbl.ROOT.Math.XYZPoint
XYZVector= gbl.ROOT.Math.XYZVector

#location = "/Event/CharmCompleteEvent/Phys/D2HHLTUnbiasedD02HHLine/Particles"
#dvlocation = "/Event/CharmCompleteEvent/Phys/D2HHLTUnbiasedD02HHLine/decayVertices"
pvLocation = "Rec/Vertex/Primary"

xmin=-5
ymin=-5
zmin=-50
xmax=5
ymax=5

HCAL_Z = 14000
MUON_Z = 19000
zmax=MUON_Z

def fourprongSequencer():
    """
    return detached four track vertices
    """
    #should filter first
    fourprong = CombineParticles('fourprong')
    fourprong.DecayDescriptor = 'B0 -> pi+ pi- pi+ pi-'
    fourprong.DaughtersCuts =   { 'pi+' : "(MIPCHI2DV(PRIMARY)>12)", 'pi-' : "(MIPCHI2DV(PRIMARY)>12)" }
    fourprong.MotherCut = "(M>1200*MeV) & (M<8000*MeV) & (BPVIPCHI2()<25) & (BPVVDCHI2>100.0) & (VFASPF(VCHI2/VDOF)<5) & (BPVDIRA>0.9)"
    fourprong.CombinationCut = "(AM>1000*MeV) & (AM<9000*MeV)" 
    stdnopids = AutomaticData("/Event/Phys/StdNoPIDsPions/Particles")    
    fourprongsel = Selection("fourprongsel", Algorithm = fourprong, RequiredSelections = [ stdnopids ])
    seq = SelectionSequence("fourprongseq", TopSelection = fourprongsel)
    seq.IgnoreFilterPassed = True
    return seq

def threeprongSequencer():
    """
    return detached three track vertices
    """
    #should filter first
    threeprong = CombineParticles('threeprong')
    threeprong.DecayDescriptor = '[B+ -> pi+ pi+ pi-]cc'
    threeprong.DaughtersCuts =   { 'pi+' : "(MIPCHI2DV(PRIMARY)>12)", 'pi-' : "(MIPCHI2DV(PRIMARY)>12)" }
    threeprong.MotherCut = "(M>1200*MeV) & (M<8000*MeV) & (BPVIPCHI2()<25) & (BPVVDCHI2>100.0) & (VFASPF(VCHI2/VDOF)<5) & (BPVDIRA>0.9)"
    threeprong.CombinationCut = "(AM>1000*MeV) & (AM<9000*MeV)" 
    stdnopids = AutomaticData("/Event/Phys/StdNoPIDsPions/Particles")    
    threeprongsel = Selection("threeprongsel", Algorithm = threeprong, RequiredSelections = [ stdnopids ])
    seq = SelectionSequence("threeprongseq", TopSelection = threeprongsel)
    seq.IgnoreFilterPassed = True
    return seq

def twoprongSequencer():
    """
    return detached two track vertices
    """
    #should filter first
    twoprong = CombineParticles('twoprong')
    twoprong.DecayDescriptor = '[B0 -> pi+ pi-]cc'
    twoprong.DaughtersCuts =   { 'pi+' : "(MIPCHI2DV(PRIMARY)>12)", 'pi-' : "(MIPCHI2DV(PRIMARY)>12)" }
    twoprong.MotherCut = "(M>1200*MeV) & (M<8000*MeV) & (BPVIPCHI2()<25) & (BPVVDCHI2>100.0) & (VFASPF(VCHI2/VDOF)<5) & (BPVDIRA>0.9)"
    twoprong.CombinationCut = "(AM>1000*MeV) & (AM<9000*MeV)" 
    stdnopids = AutomaticData("/Event/Phys/StdNoPIDsPions/Particles")    
    twoprongsel = Selection("twoprongsel", Algorithm = twoprong, RequiredSelections = [ stdnopids ])
    seq = SelectionSequence("twoprongseq", TopSelection = twoprongsel)
    seq.IgnoreFilterPassed = True
    return seq

#DaVinci().UserAlgorithms = [ fourprongSequencer() ]


class EventCache:
  """ Util class to keep some event state """
  
  def __init__(self):
    self.tracksdone = []

    
class GaudiManager:
  """ Wrap Gaudi calls """
  
  def __init__(self, nbevents=-1, simulation=False, datatype='2012'):
    self.nbevents = nbevents
    self.simulation = simulation
    self.datatype = datatype
    self.startGaudi()

  #
  # GAUDI Setup & iterator
  #
  ##############################################################################
  def startGaudi(self): 
    dv = DaVinci()
    dv.DDDBtag = 'default'
    dv.CondDBtag = 'default'
    dv.DataType = self.datatype
    dv.EvtMax = self.nbevents
    dv.HistogramFile = 'histos.root'
    dv.TupleFile = 'tuples.root'
    dv.HistogramFile = ""
    dv.TupleFile = ""
    self.dv = dv
    
    #run selections first
    #??? !!! reactivate
    dv.UserAlgorithms = [ fourprongSequencer(), threeprongSequencer(), twoprongSequencer() ]

    # Phase 2: GaudiPython
    from GaudiPython.Bindings import gbl, AppMgr, Helper
    appMgr = AppMgr(outputlevel=4)
    appMgr.initialize()
    self.appMgr = appMgr
    
    import atexit
    atexit.register(appMgr.exit)

    # some short-cuts to services
    self.evtSvc = appMgr.evtSvc()
    self.detSvc = appMgr.detSvc()
    self.toolSvc = appMgr.toolsvc()
    self.histSvc = appMgr.histSvc()
    self.evtSel = appMgr.evtSel()
    self.ppSvc = appMgr.ppSvc()
    self.tExp = self.toolSvc.create("TrackMasterExtrapolator", interface="ITrackExtrapolator")
    self.relPVFinder = self.toolSvc.create(DVTools.P2PVWithIPChi2, interface = 'IRelatedPVFinder')
    self.distanceCalculator = self.toolSvc.create('LoKi::DistanceCalculator' , interface='IDistanceCalculator') 
    self.nextEvent = Functors.NextEvent(appMgr, appMgr.EvtMax)
    self.evtCount=0

    #self.dtf = self.decayTreeFitter('LoKi::DecayTreeFit')
    #print self.dtf.name()


  def stopGaudi(self):
    self.appMgr.stop()
    self.appMgr.finalize()


  def next(self):
    return self.nextEvent()


  #
  # Tools
  #
  ##############################################################################
  def propagateTrack(self, t, zstep, zstart,  zfinal):
    """ Propagate the track in the detector and return a TPointsArray3D"""
    #print "Propagate track called with zfinal:", zfinal
    trackjson = []
    s = t.firstState().clone()
    #p = s.position()
    #z = zstart
    #sgn = (zfinal - zstep) /(abs(zfinal - zstep))
    #idx = 0
    #print "--- writeTrack --- zstart: %s zfinal:%s zstep: %s " % (zstart, zfinal, zstep)
    #while (sgn * z < sgn * zfinal and p.x() < xmax and p.y() < ymax):
    npoints = 10
    #zstep = 
    for i in range(0, npoints+1):
      z = zstart + i*(zfinal-zstart)/npoints
      self.tExp.propagate(t, z, s)
      p = s.position()
      trackjson.append(("%.4f" % p.x(), "%.4f" % p.y(), "%.4f" % p.z()))
    
    #while (sgn * z < sgn * zfinal):
    #  self.tExp.propagate(t, z, s)
    #  p = s.position()
    #  #trackjson.append((int(p.x()), int(p.y()), int(p.z())))
    #  #"%.2f" % 
    #  trackjson.append(("%.2f" % p.x(), "%.2f" % p.y(), "%.2f" % p.z()))
    #  z = z + self.getStep(zstep, z)
    #  idx = idx + 1
    
    return  trackjson


  def getImpactParameter(self, particle, vertex):
    """ Wraps up the IDistanceCalculator """
    ip=Double(0)
    ipchi2=Double(0)
    res = self.distanceCalculator.distance(particle, vertex, ip, ipchi2)
    if res.isFailure(): 
      return None
    else: 
      return (ip, ipchi2)


  def getTrackFirstMeasurementZ(self, track):

    if track == None:
      return None

    z = None
    for s in track.states():
      if s.LocationToString(s.location()) == "FirstMeasurement":
        z = s.z()
        break
    return z

  def getStep(self, defaultVal, z):
    """ Adapt the step according to the position in the detector """
    retval = 200
    if z < 100:
      retval = 5
    elif z < 1000:
      retval = 20
    elif z < 10000:
      retval = 100
      
    return retval
    
  def writeLine(self, x0, y0, z0, x1, y1, z1):
    """ Create a TPointsArray3D with the given coordinates """
    trackjson = []
    trackjson.append(( int(x0), int(y0), int(z0)))
    trackjson.append(( int(x1), int(y1), int(z1)))
    return trackjson

  def findBestVertex(self, particle):
    primaryVertices =  self.evtSvc[pvLocation] 
    if (primaryVertices.size()==0):
        p = XYZPoint(0.0, 0.0, -500.0)
        recv = LHCb.RecVertex(p)
        return recv
    table = self.relPVFinder.relatedPVs(particle, primaryVertices)
    relPVs = table.relations(particle)
    bestVertex = relPVs.back().to()
    return bestVertex

  #
  # Data extraction
  #
  ##############################################################################
  def processParticle(self, strname, event, p, lvl, r0):
    """ Dump a particle and its daughters to the file """

    print "########### Processing particle: "
    #print p
    
    particles = []
    allparticlesjson = []
    # First checking if this particle was already processed
    if p.proto() != None and p.proto().track() != None:
      if  p.proto().track() in event.tracksdone:
        print "Particle already processed:", p
        return (particles, allparticlesjson)

    # Stop all partciles at HCAL, except muons
    zfinal = HCAL_Z
    #if p.particleID().pid() == 13:
    if (strname == "mu+" or strname == "mu-"):
      zfinal = MUON_Z

    p1 = None
    if p.endVertex() != None:
      p1 = p.endVertex().position()
      zfinal = p1.z()

    # Extracting particle PID
    #pid = p.particleID()
    #pinfo = self.ppSvc.find(pid)
    #strname = pname #pinfo.name()

    particlejson = {}
    particlejson['name'] = strname
    particlejson['m'] = p.measuredMass()
    particlejson['E'] =  p.momentum().E()
    particlejson['px'] = p.momentum().px()
    particlejson['py'] = p.momentum().py()
    particlejson['pz'] = p.momentum().pz()
    particlejson['q'] = p.charge()

    if p.proto() != None and p.proto().track() != None:
      particlejson['zFirstMeasurement'] = self.getTrackFirstMeasurementZ(p.proto().track())
    
    # Calculating the IP and IPChi2 to the best vertex
    vertex = self.findBestVertex(p)
    particlejson['pv_x'] = vertex.position().x()
    particlejson['pv_y'] = vertex.position().y()
    particlejson['pv_z'] = vertex.position().z()
    
    res = self.getImpactParameter(p, vertex)
    if res != None:
      particlejson['ipchi2'] = res[1]
      particlejson['ip'] = res[0]
    
    # Preparing the track for export
    myjsontrack = None
    if p.proto() != None:
      particlejson['trackchi2'] = p.proto().track().chi2PerDoF()
      tjson = self.propagateTrack(p.proto().track(), 5, r0[2], zfinal)
      event.tracksdone.append(p.proto().track())
      myjsontrack = tjson
    else:
      if p1 != None:
        tjson  = self.writeLine(r0[0], r0[1], r0[2], p1.x(), p1.y(), p1.z())
        myjsontrack = tjson
      else:
        raise Exception("Cannot calculate track for this particle")

    # Checking the track and adding it to the particle
    if myjsontrack == None:
      print "Adding Particle Error: Null track - Ignoring"
    elif  len(myjsontrack) == 0:
      print "Adding particle Error: Track with no points - Ignoring"
    else:
      # Now add the the object to the array
      particlejson["track"] = myjsontrack
      allparticlesjson.append(particlejson)
      print "Added particle"

    
    # Now iterate on the particle daughters
    for td in p.daughters():
      # Careful we need to use the .target() link not .data()!!!
      print "Daughter class", td.__class__.__name__
      if td.__class__.__name__ != "LHCb::Particle":
        dd = td.target()
      else:
        dd = td
      if dd != None:
        subpartsjson = self.processParticle(event, dd, lvl + 1, (p1.x(), p1.y(), p1.z()))
        allparticlesjson += subpartsjson
        
    return allparticlesjson

  def getTrackVeloClusters(track, veloClusters):
    clusters = []
    # Iterating on LHCbIDs
    points = []
    phipoints = []
    print "################################### Number of lhcbIDs:", len(track.lhcbIDs())
    print "###################################"
    print "###################################"
    print "###################################"
    for (idx, l) in enumerate(track.lhcbIDs()):
      print "###################################", idx , " # ", l.isVelo()
      if not l.isVelo():
        continue
      print "##############", idx , " # ", l.isVelo()
      lv = l.veloID()
      cl = veloClusters.containedObject(lv.channelID())
      print "############## PhiType:", cl.isPhiType()
      print "############## RType:  ", cl.isRType()
      if cl.isRType():
        meas = VeloRMeasurement(cl, velo, tVeloClusterPos )
        traj = meas.trajectory()
        rangeMin = traj.beginRange()
        rangeMax = traj.endRange()
        nPoints = 20;
        step = (rangeMax - rangeMin) / nPoints
        iPos = rangeMin
        m = []
        while(iPos < rangeMax):
          iPos += step
          pos = traj.position(iPos)
          m.append(pos)
        points.append(m)
      elif cl.isPhiType():
        print "--------------------------------------> Adding Velo Phi meas"
        meas = VeloPhiMeasurement(cl, velo, tVeloClusterPos )
        traj  = meas.trajectory()
        m = [traj.beginPoint(), traj.endPoint()]
        phipoints.append(m)

    # Now returning
    return (points, phipoints)


  def displayHits():
    veloClusters = evtSvc['Raw/Velo/Clusters']
    for i, t in enumerate(tracks):
      #print "Track %s" % i
      if t.chi2PerDoF() > 5:
        print "Skipping track: ", t
        continue

      (lhits, lphihits)  = getTrackVeloClusters(t, veloClusters)
      if lhits != None:
        print "Found %d hits" % len(lhits)
        for h in lhits:
          writeHits(h)
      else:
        print "No hits"

      if lphihits != None:
        print "Found %d phi hits" % len(lhits)
        for h in lphihits:
          writeHits(h, kGreen)


  #
  # Main extraction method 
  #
  ##############################################################################
  def extractData(self, event):
    
    evtSvc = self.evtSvc
    #primVertices = evtSvc['/Event/CharmCompleteEvent/Rec/Vertex/Primary'] #TODO ??? !!!
    #primVertices = evtSvc['/Event/Rec/Vertex/Primary'] #TODO ??? !!!
    ptracks = []

    eventjson = {}
    
    #"Rec/Vertex/Primary"
    #veloclusters = evtSvc["/Event/Raw/Odin"]
    daqodin = evtSvc["DAQ/ODIN"]
    print "eventNumber",daqodin.eventNumber()
    print "runNumber",daqodin.runNumber()
    print "gpsTime",daqodin.gpsTime()
    eventjson["eventNumber"] = daqodin.eventNumber()
    eventjson["runNumber"] = daqodin.runNumber()
    eventjson["gpsTime"] = daqodin.gpsTime()
    from time import gmtime, strftime, localtime
    #timestr = strftime("%a, %d %b %Y %H:%M:%S", daqodin.gpsTime())
    timestr = strftime("%a, %d %b %Y %H:%M:%S", localtime(daqodin.gpsTime()/1000000))
    print timestr
    #gpstime mus since 1970 
    eventjson["time"] = timestr

    #get the hits
    #velohits = "/Event/Raw/Velo/" #Clusters
    veloclusters = evtSvc["/Event/Raw/Velo/Clusters"]
    ttclusters = evtSvc["/Event/Raw/TT/Clusters"]
    #otclusters = evtSvc["/Event/Raw/OT/Clusters"]
    otclusters = evtSvc["/Event/Raw/OT/Times"]
    itclusters = evtSvc["/Event/Raw/IT/Clusters"]
    hcaldigits = evtSvc["/Event/Raw/Hcal/Digits"]
    ecaldigits = evtSvc["/Event/Raw/Ecal/Digits"]
    muoncoords = evtSvc["/Event/Raw/Muon/Coords"]

    #det = appMgr.detSvc()
    detSvc = self.detSvc
    velodet = detSvc['/dd/Structure/LHCb/BeforeMagnetRegion/Velo'] #This is a VeloDet object 
    #dir(velodet)
    muondet = detSvc['/dd/Structure/LHCb/DownstreamRegion/Muon']
    ecaldet = detSvc['/dd/Structure/LHCb/DownstreamRegion/Ecal']
    hcaldet = detSvc['/dd/Structure/LHCb/DownstreamRegion/Hcal']
    otdet = detSvc['/dd/Structure/LHCb/AfterMagnetRegion/T/OT']
    itdet = detSvc['/dd/Structure/LHCb/AfterMagnetRegion/T/IT']
    ttdet = detSvc['/dd/Structure/LHCb/BeforeMagnetRegion/TT']


    #
    # Adding the Muon hits to the event representation
    ################################################################
    print "PRIMARY VERTICES"
    pvs = []
    
    for pv in evtSvc["/Event/Rec/Vertex/Primary"] :
      pvjson = {}
      mdata = array('d', [pv.covMatrix().At(0,0), pv.covMatrix().At(0,1), pv.covMatrix().At(0,2), 
                          pv.covMatrix().At(1,0), pv.covMatrix().At(1,1), pv.covMatrix().At(1,2),
                          pv.covMatrix().At(2,0), pv.covMatrix().At(2,1), pv.covMatrix().At(2,2)])
      msym = TMatrixDSym(3)      
      msym.SetMatrixArray(mdata)
      meigen = TMatrixDSymEigen(msym)
      eigenvalues = meigen.GetEigenValues()
      eigenvectors = meigen.GetEigenVectors()      
      pvjson['pv_x'] = pv.position().x()
      pvjson['pv_y'] = pv.position().y()
      pvjson['pv_z'] = pv.position().z()
      pvjson['eval_0'] = eigenvalues(0)
      pvjson['eval_1'] = eigenvalues(1)
      pvjson['eval_2'] = eigenvalues(2)
      mag0 = math.sqrt(eigenvectors(0,0)*eigenvectors(0,0)+eigenvectors(1,0)*eigenvectors(1,0)+eigenvectors(2,0)*eigenvectors(2,0))
      mag1 = math.sqrt(eigenvectors(0,1)*eigenvectors(0,1)+eigenvectors(1,1)*eigenvectors(1,1)+eigenvectors(2,1)*eigenvectors(2,1))
      mag2 = math.sqrt(eigenvectors(0,2)*eigenvectors(0,2)+eigenvectors(1,2)*eigenvectors(1,2)+eigenvectors(2,2)*eigenvectors(2,2))
      pvjson['evec_0x'] = eigenvectors(0,0)/mag0
      pvjson['evec_0y'] = eigenvectors(1,0)/mag0
      pvjson['evec_0z'] = eigenvectors(2,0)/mag0
      pvjson['evec_1x'] = eigenvectors(0,1)/mag1
      pvjson['evec_1y'] = eigenvectors(1,1)/mag1
      pvjson['evec_1z'] = eigenvectors(2,1)/mag1
      pvjson['evec_2x'] = eigenvectors(0,2)/mag2
      pvjson['evec_2y'] = eigenvectors(1,2)/mag2
      pvjson['evec_2z'] = eigenvectors(2,2)/mag2      
      pvs.append(pvjson)
    print "Primary Vertices",evtSvc["/Event/Rec/Vertex/Primary"].size()
    
    eventjson["PVS"] = pvs

    #
    # Adding the Muon hits to the event representation
    ################################################################
    print "MUON"
    muonhits = []
    try:
        for d in evtSvc["/Event/Raw/Muon/Coords"] :
            for tile in d.digitTile() :
        #x = dx = y = dy = z = dz = 0.0
                x = Double(0.0)
        dx = Double(0.0)
        y = Double(0.0)
        dy = Double(0.0)
        z = Double(0.0)
        dz = Double(0.0)
        #x =0.0; dx = 0.0; y = 0.0; dy = 0.0; z = 0.0; dz = 0.0;
        muondet.Tile2XYZ(tile, x, dx, y, dy, z, dz)
        #print x,dx,y,dy,z,dz
        #muonhits.append((float(x),float(dx),float(y),float(dy),float(z),float(dz)))
        muonhits.append((int(x),int(dx),int(y),int(dy),int(z),int(dz)))
        #layout = tile.layout
        print "Muon Digits",evtSvc["/Event/Raw/Muon/Coords"].size()
    except:
        print("Could not find Muon hits")
    eventjson["MUON"] = muonhits

    #
    # Adding the OT hits to the event representation
    ################################################################
    print "OT"
    othits = []
    for d in evtSvc["/Event/Raw/OT/Times"] :
      cid = d.channel()
      #module = otdet.findModule(cid)
      lhcbid  = gbl.LHCb.LHCbID(cid)
      traj = otdet.trajectory(lhcbid,0.0);
      #print traj.beginPoint(),traj.endPoint()
      #othits.append((traj.beginPoint().x(), traj.beginPoint().y(), traj.beginPoint().z(),
      #               traj.endPoint().x(), traj.endPoint().y(), traj.endPoint().z()))
      othits.append((int(traj.beginPoint().x()), int(traj.beginPoint().y()), int(traj.beginPoint().z()),
                     int(traj.endPoint().x()), int(traj.endPoint().y()), int(traj.endPoint().z())))
    print "OtTimes",evtSvc["/Event/Raw/OT/Times"].size()
    eventjson["OT"] = othits

    #
    # Adding the IT hits to the event representation
    ################################################################
    print "IT"
    ithits = []
    for d in evtSvc["/Event/Raw/IT/Clusters"] :
      cid = d.channelID()
      lhcbid  = gbl.LHCb.LHCbID(cid)      
      traj = itdet.trajectory(lhcbid,0.0);
      #print traj.beginPoint(),traj.endPoint()
      #ithits.append((traj.beginPoint().x(), traj.beginPoint().y(), traj.beginPoint().z(),
      #               traj.endPoint().x(), traj.endPoint().y(), traj.endPoint().z()) )
      ithits.append((int(traj.beginPoint().x()), int(traj.beginPoint().y()), int(traj.beginPoint().z()),
                     int(traj.endPoint().x()), int(traj.endPoint().y()), int(traj.endPoint().z())) )
    print "ItCluster",evtSvc["/Event/Raw/IT/Clusters"].size()
    eventjson["IT"] = ithits

    #
    # Adding the IT hits to the event representation
    ################################################################
    print "TT"
    tthits = []
    for d in evtSvc["/Event/Raw/TT/Clusters"] :
      cid = d.channelID()
      lhcbid  = gbl.LHCb.LHCbID(cid)      
      traj = ttdet.trajectory(lhcbid,0.0);
      #print traj.beginPoint(),traj.endPoint()
      #ithits.append((traj.beginPoint().x(), traj.beginPoint().y(), traj.beginPoint().z(),
      #               traj.endPoint().x(), traj.endPoint().y(), traj.endPoint().z()) )
      tthits.append((int(traj.beginPoint().x()), int(traj.beginPoint().y()), int(traj.beginPoint().z()),
                     int(traj.endPoint().x()), int(traj.endPoint().y()), int(traj.endPoint().z())) )
    print "TtCluster",evtSvc["/Event/Raw/TT/Clusters"].size()
    eventjson["TT"] = tthits

    #
    # Adding the Velo hits to the event representation
    ################################################################
    print "VELO"
    print "NumClusters",evtSvc['/Event/Raw/Velo/Clusters'].size()
    numstrips = 0
    velophi = []
    velor = []
    for d in evtSvc['/Event/Raw/Velo/Clusters'] : #d is a VeloCluster object, d.isRType, d.isPhiType
      cid = d.channelID() 
      #lhcbid  = gbl.LHCb.LHCbID(cid)
      #type ="PHI"
      #if d.isRType():
      #  type = "R"
      velosensor = velodet.sensor(cid)#This is a VeloSensor
      for sv in d.stripValues() :
        striplimits = velosensor.globalStripLimits(sv.first) #this will not be 100% correct, but should suffice... otherwise go via trajectory i guess
        #print striplimits
        f = striplimits.first
        s = striplimits.second
        #f = velosensor.globalStripLimits(sv.first).first
        #s = velosensor.globalStripLimits(sv.first).second
        ##velohits.append((f.x(), f.y(), f.z(), s.x(), s.y(), s.z()) )
        if (d.isRType() or d.isPileUp()):
            velor.append((int(f.x()), int(f.y()), int(f.z()), int(s.x()), int(s.y()), int(s.z())) )
        else:
            velophi.append((int(f.x()), int(f.y()), int(f.z()), int(s.x()), int(s.y()), int(s.z())) )        
    print "NumStrips",evtSvc['/Event/Raw/Velo/Clusters'].size()
    eventjson["VELOPHI"] = velophi
    eventjson["VELOR"] = velor


    #
    # Adding the HCAL hits to the event representation
    ################################################################    
    print "HCAL"
    hcalhits = []
    for d in evtSvc['/Event/Raw/Hcal/Digits'] :
      if d.e() > 150 :
        cid =  d.cellID()
        x = hcaldet.cellX( cid )
        y = hcaldet.cellY( cid )
        z = hcaldet.cellZ( cid )
        s = hcaldet.cellSize( cid )
        #print hcaldet.cellSize( cid )
        #print "HCAL",d.e(),x,y,z
        #hcalhits.append((d.e(),x,y,z))
        hcalhits.append((int(d.e()),int(x),int(y),int(z),int(s)))
    #print "Hcal digits",evtSvc['/Event/Raw/Hcal/Digits'].size()
    eventjson["HCAL"] = hcalhits

    #
    # Adding the ECAL hits to the event representation
    ################################################################    
    print "ECAL"
    ecalhits = []
    for d in evtSvc['/Event/Raw/Ecal/Digits'] :
      if d.e() > 150 :
        cid =  d.cellID()
        x = ecaldet.cellX( cid )
        y = ecaldet.cellY( cid )
        z = ecaldet.cellZ( cid )
        s = ecaldet.cellSize( cid )
        #print ecaldet.cellSize( cid )
        #print "ECAL",d.e(),x,y,z
        #ecalhits.append((float(d.e()),float(x),float(y),float(z)))
        ecalhits.append((int(d.e()),int(x),int(y),int(z),int(s)))
    #print "Ecal digits",evtSvc['/Event/Raw/Ecal/Digits'].size()
    eventjson["ECAL"] = ecalhits
    
    #Adding VeloTracks
    vtracks = []
    for t in evtSvc['/Event/Rec/Track/Best']:
        print "TRACK WITH TYPE",t.type()
        atrack = []
        if t.checkType(int(1)): #velo tracks
            pos = t.position() #first state
            m = t.momentum() #direction at first state
            mnormalized = m/math.sqrt(m.Mag2())
            #determine zstart (z position of associated vertex)
            zstart = 0.0
            ip = 1.0e+6
            closest = XYZPoint() #pos #pointer!!!
            closest.SetXYZ(0.0, 0.0, 0.0)
            print pos,m,mnormalized
            for pv in evtSvc["/Event/Rec/Vertex/Primary"] : #loop over all pvs to find the one with the smallest ip wrt this velotrack
                pvpos = pv.position()
                pvminuspos = XYZVector() 
                pvminuspos.SetXYZ(pvpos.x()-pos.x(), pvpos.y()-pos.y(), pvpos.z()-pos.z())
                currentip = math.sqrt(  (pvminuspos - pvminuspos.Dot(mnormalized)*mnormalized).Mag2() )
                if currentip < ip:
                    closest = pos + pvminuspos.Dot(mnormalized)*mnormalized
                    zstart = closest.z()
                    ip = currentip
            zfinal = (closest+mnormalized*500.0).z()
            if t.checkFlag(int(1)): #backwards track
                zfinal = (closest-mnormalized*500.0).z()            
            #print "from",zstart,"to",zfinal,"firststate",pos.z(),"slope",t.slopes(),"rapidity",t.pseudoRapidity()
            s = t.firstState().clone()
            npoints = 5
            for i in range(0,npoints+1):                
                z = zstart + i*(zfinal-zstart)/npoints
                self.tExp.propagate(t, z, s)
                p = s.position()
                atrack.append(("%.4f" % p.x(), "%.4f" % p.y(), "%.4f" % p.z()))                
            vtracks.append(atrack)
    eventjson["VTRACKS"] = vtracks

    
    #
    # Adding the particles and their tracks
    ################################################################    
    json = []
    RichDLLmu = 101
    RichDLLpi = 102
    RichDLLk  = 103
    ProbNNe = 700  # The ANN probability for the electron hypothesis
    ProbNNmu = 701 # The ANN probability for the muon hypothesis
    ProbNNpi = 702 # The ANN probability for the pion hypothesis
    ProbNNk = 703  # The ANN probability for the kaon hypothesis
    ProbNNp = 704  # The ANN probability for the proton hypothesis
    ProbNNghost = 705# The ANN probability for the ghost hypothesis
    
    #pions = evtSvc['/Event/Phys/StdNoPIDsPions/Particles']
    pions = evtSvc['/Event/Phys/StdAllNoPIDsPions/Particles']
    #pions = evtSvc['Phys/StdNoPIDsPions/Particles']
    print "STDNOPIDPIONS ",pions.size()
    #pions = evtSvc['/Event/Phys/StdLoosePions/Particles']
    for p in pions:
      #print "HAVE PION"
      pname = ""
      if (p.charge() > 0):
          pname = "pi+"
      if  (p.proto().info(ProbNNk, -9999) > 0.3 and p.proto().info(ProbNNk, -9999) > p.proto().info(ProbNNp, -9999) and p.proto().info(ProbNNk, -9999) > p.proto().info(ProbNNe, -9999) and p.proto().info(ProbNNk, -9999) > p.proto().info(ProbNNmu, -9999)):
          pname = "K+"
      if  (p.proto().info(ProbNNp, -9999) > 0.3 and p.proto().info(ProbNNp, -9999) > p.proto().info(ProbNNk, -9999) and p.proto().info(ProbNNp, -9999) > p.proto().info(ProbNNe, -9999) and p.proto().info(ProbNNp, -9999) > p.proto().info(ProbNNmu, -9999)):
          pname = "p+"
      if  (p.proto().info(ProbNNe, -9999) > 0.3 and p.proto().info(ProbNNe, -9999) > p.proto().info(ProbNNk, -9999) and p.proto().info(ProbNNe, -9999) > p.proto().info(ProbNNp, -9999) and p.proto().info(ProbNNe, -9999) > p.proto().info(ProbNNmu, -9999)):
          pname = "e+"        
      if  (p.proto().info(ProbNNmu, -9999) > 0.1 and p.proto().muonPID().IsMuon()):
          pname = "mu+"        
      if (p.charge() < 0):
        pname = "pi-"
        if  (p.proto().info(ProbNNk, -9999) > 0.3 and p.proto().info(ProbNNk, -9999) > p.proto().info(ProbNNp, -9999) and p.proto().info(ProbNNk, -9999) > p.proto().info(ProbNNe, -9999) and p.proto().info(ProbNNk, -9999) > p.proto().info(ProbNNmu, -9999)):
          pname = "K-"
        if  (p.proto().info(ProbNNp, -9999) > 0.3 and p.proto().info(ProbNNp, -9999) > p.proto().info(ProbNNk, -9999) and p.proto().info(ProbNNp, -9999) > p.proto().info(ProbNNe, -9999) and p.proto().info(ProbNNp, -9999) > p.proto().info(ProbNNmu, -9999)):
          pname = "p-"
        if  (p.proto().info(ProbNNe, -9999) > 0.3 and p.proto().info(ProbNNe, -9999) > p.proto().info(ProbNNk, -9999) and p.proto().info(ProbNNe, -9999) > p.proto().info(ProbNNp, -9999) and p.proto().info(ProbNNe, -9999) > p.proto().info(ProbNNmu, -9999)):
          pname = "e-"        
        if  (p.proto().info(ProbNNmu, -9999) > 0.1 and p.proto().muonPID().IsMuon()):
          pname = "mu-"        
      #print "ProbNNpi:", p.proto().info(ProbNNpi, -9999)
      pos = self.findBestVertex(p).position()      
      #print "BESTVERTEX FOR PION ",pos.x(),",",pos.y(),",",pos.z()
      #tmpjson =  self.processParticle(pname, event, p, 0, (pos.x(), pos.y(), -100))
      tmpjson =  self.processParticle(pname, event, p, 0, (pos.x(), pos.y(), pos.z()))
      json += tmpjson
    eventjson["PARTICLES"] = json
    
    #pick up combined detached 2,3 and 4 prong vertices
    #fourprongs = evtSvc['/Event/Phys/fourprong/Particles']
    
    svsfourprong = []
    fourprongs = evtSvc[ '/Event/Phys/fourprongsel/Particles' ]
    for p in fourprongs:
      print "FOURPRONG"
      svjson = {}
      sv = p.endVertex()
      #print "ISSUE?"
      mdata = array('d', [sv.covMatrix().At(0,0), sv.covMatrix().At(0,1), sv.covMatrix().At(0,2), 
                          sv.covMatrix().At(1,0), sv.covMatrix().At(1,1), sv.covMatrix().At(1,2),
                          sv.covMatrix().At(2,0), sv.covMatrix().At(2,1), sv.covMatrix().At(2,2)])
      msym = TMatrixDSym(3)      
      msym.SetMatrixArray(mdata)
      meigen = TMatrixDSymEigen(msym)
      eigenvalues = meigen.GetEigenValues()
      eigenvectors = meigen.GetEigenVectors()      
      svjson['sv_x'] = sv.position().x()
      svjson['sv_y'] = sv.position().y()
      svjson['sv_z'] = sv.position().z()
      svjson['eval_0'] = eigenvalues(0)
      svjson['eval_1'] = eigenvalues(1)
      svjson['eval_2'] = eigenvalues(2)
      mag0 = math.sqrt(eigenvectors(0,0)*eigenvectors(0,0)+eigenvectors(1,0)*eigenvectors(1,0)+eigenvectors(2,0)*eigenvectors(2,0))
      mag1 = math.sqrt(eigenvectors(0,1)*eigenvectors(0,1)+eigenvectors(1,1)*eigenvectors(1,1)+eigenvectors(2,1)*eigenvectors(2,1))
      mag2 = math.sqrt(eigenvectors(0,2)*eigenvectors(0,2)+eigenvectors(1,2)*eigenvectors(1,2)+eigenvectors(2,2)*eigenvectors(2,2))
      svjson['evec_0x'] = eigenvectors(0,0)/mag0
      svjson['evec_0y'] = eigenvectors(1,0)/mag0
      svjson['evec_0z'] = eigenvectors(2,0)/mag0
      svjson['evec_1x'] = eigenvectors(0,1)/mag1
      svjson['evec_1y'] = eigenvectors(1,1)/mag1
      svjson['evec_1z'] = eigenvectors(2,1)/mag1
      svjson['evec_2x'] = eigenvectors(0,2)/mag2
      svjson['evec_2y'] = eigenvectors(1,2)/mag2
      svjson['evec_2z'] = eigenvectors(2,2)/mag2      
      pv = self.findBestVertex(p)
      svjson['pv_x'] = pv.position().x()
      svjson['pv_y'] = pv.position().y()
      svjson['pv_z'] = pv.position().z()
      svsfourprong.append(svjson)
    eventjson["FOURPRONG"] = svsfourprong

    svsthreeprong = []
    threeprongs = evtSvc[ '/Event/Phys/threeprongsel/Particles' ]
    for p in threeprongs:
      print "THREEPRONG"
      svjson = {}
      sv = p.endVertex()
      mdata = array('d', [sv.covMatrix().At(0,0), sv.covMatrix().At(0,1), sv.covMatrix().At(0,2), 
                          sv.covMatrix().At(1,0), sv.covMatrix().At(1,1), sv.covMatrix().At(1,2),
                          sv.covMatrix().At(2,0), sv.covMatrix().At(2,1), sv.covMatrix().At(2,2)])
      msym = TMatrixDSym(3)      
      msym.SetMatrixArray(mdata)
      meigen = TMatrixDSymEigen(msym)
      eigenvalues = meigen.GetEigenValues()
      eigenvectors = meigen.GetEigenVectors()      
      svjson['sv_x'] = sv.position().x()
      svjson['sv_y'] = sv.position().y()
      svjson['sv_z'] = sv.position().z()
      svjson['eval_0'] = eigenvalues(0)
      svjson['eval_1'] = eigenvalues(1)
      svjson['eval_2'] = eigenvalues(2)
      mag0 = math.sqrt(eigenvectors(0,0)*eigenvectors(0,0)+eigenvectors(1,0)*eigenvectors(1,0)+eigenvectors(2,0)*eigenvectors(2,0))
      mag1 = math.sqrt(eigenvectors(0,1)*eigenvectors(0,1)+eigenvectors(1,1)*eigenvectors(1,1)+eigenvectors(2,1)*eigenvectors(2,1))
      mag2 = math.sqrt(eigenvectors(0,2)*eigenvectors(0,2)+eigenvectors(1,2)*eigenvectors(1,2)+eigenvectors(2,2)*eigenvectors(2,2))
      svjson['evec_0x'] = eigenvectors(0,0)/mag0
      svjson['evec_0y'] = eigenvectors(1,0)/mag0
      svjson['evec_0z'] = eigenvectors(2,0)/mag0
      svjson['evec_1x'] = eigenvectors(0,1)/mag1
      svjson['evec_1y'] = eigenvectors(1,1)/mag1
      svjson['evec_1z'] = eigenvectors(2,1)/mag1
      svjson['evec_2x'] = eigenvectors(0,2)/mag2
      svjson['evec_2y'] = eigenvectors(1,2)/mag2
      svjson['evec_2z'] = eigenvectors(2,2)/mag2      
      pv = self.findBestVertex(p)
      svjson['pv_x'] = pv.position().x()
      svjson['pv_y'] = pv.position().y()
      svjson['pv_z'] = pv.position().z()
      svsthreeprong.append(svjson)
    eventjson["THREEPRONG"] = svsthreeprong

    svstwoprong = []
    twoprongs = evtSvc[ '/Event/Phys/twoprongsel/Particles' ]
    for p in twoprongs:
      print "TWOPRONG"
      svjson = {}
      sv = p.endVertex()
      mdata = array('d', [sv.covMatrix().At(0,0), sv.covMatrix().At(0,1), sv.covMatrix().At(0,2), 
                          sv.covMatrix().At(1,0), sv.covMatrix().At(1,1), sv.covMatrix().At(1,2),
                          sv.covMatrix().At(2,0), sv.covMatrix().At(2,1), sv.covMatrix().At(2,2)])
      msym = TMatrixDSym(3)      
      msym.SetMatrixArray(mdata)
      meigen = TMatrixDSymEigen(msym)
      eigenvalues = meigen.GetEigenValues()
      eigenvectors = meigen.GetEigenVectors()      
      svjson['sv_x'] = sv.position().x()
      svjson['sv_y'] = sv.position().y()
      svjson['sv_z'] = sv.position().z()
      svjson['eval_0'] = eigenvalues(0)
      svjson['eval_1'] = eigenvalues(1)
      svjson['eval_2'] = eigenvalues(2)
      mag0 = math.sqrt(eigenvectors(0,0)*eigenvectors(0,0)+eigenvectors(1,0)*eigenvectors(1,0)+eigenvectors(2,0)*eigenvectors(2,0))
      mag1 = math.sqrt(eigenvectors(0,1)*eigenvectors(0,1)+eigenvectors(1,1)*eigenvectors(1,1)+eigenvectors(2,1)*eigenvectors(2,1))
      mag2 = math.sqrt(eigenvectors(0,2)*eigenvectors(0,2)+eigenvectors(1,2)*eigenvectors(1,2)+eigenvectors(2,2)*eigenvectors(2,2))
      svjson['evec_0x'] = eigenvectors(0,0)/mag0
      svjson['evec_0y'] = eigenvectors(1,0)/mag0
      svjson['evec_0z'] = eigenvectors(2,0)/mag0
      svjson['evec_1x'] = eigenvectors(0,1)/mag1
      svjson['evec_1y'] = eigenvectors(1,1)/mag1
      svjson['evec_1z'] = eigenvectors(2,1)/mag1
      svjson['evec_2x'] = eigenvectors(0,2)/mag2
      svjson['evec_2y'] = eigenvectors(1,2)/mag2
      svjson['evec_2z'] = eigenvectors(2,2)/mag2      
      pv = self.findBestVertex(p)
      svjson['pv_x'] = pv.position().x()
      svjson['pv_y'] = pv.position().y()
      svjson['pv_z'] = pv.position().z()
      svstwoprong.append(svjson)
    eventjson["TWOPRONG"] = svstwoprong
     
    return eventjson

if __name__ == '__main__':

  parser = OptionParser()
  parser.add_option("-n", "--nbevents", dest="nbevents",
                    default="-1",
                    help="specify the number of events to process")
  parser.add_option("--datatype", dest="datatype",
                    default="2012",
                    help="specify the datatype")
  parser.add_option("--simulation", dest="simulation",
                    default=False,
                    help="specify whether this is simulated data")

  (options, args) = parser.parse_args()

  if len(args) < 1:
    print "Please specify name of DST file"
    exit(1)

  filename = args[0]
  from GaudiConf import IOHelper
  IOHelper('ROOT').inputFiles([
    filename
    ], clear=True)
  FileCatalog().Catalogs += [ 'xmlcatalog_file:charmcomplete.xml' ]
  
  filenames = []
  # Now starting gaudi
  gmgr = GaudiManager(options.nbevents, options.simulation, options.datatype)
  index = 0
  #new event selection
  evt = gmgr.evtSvc #AppMgr.evtsvc()
  #end new event selection
  while gmgr.next():
    index += 1
    #new event selection
    cont = evt['Rec/Header']    
    #if 70681==cont.runNumber() and 2000==cont.evtNumber() :
    #if 2806078611!=cont.evtNumber() :
    #  continue;
    #end new event selection
    print "#  Event: %d" % index
    print "############################################################"
    # First some setup
    event = EventCache()
    jsondata = gmgr.extractData(event)
    #print jsondata
    #with open("event_%d.json" % index, 'w') as outfile:
    filename = str(jsondata["runNumber"])+"_"+str(jsondata["eventNumber"])+".json"
    filenames.append("events/"+filename)
    with open(filename, 'w') as outfile:
      json.dump(jsondata, outfile)
    

  with open("events.json", 'w') as outfile:
    json.dump(filenames, outfile)
  gmgr.stopGaudi()

